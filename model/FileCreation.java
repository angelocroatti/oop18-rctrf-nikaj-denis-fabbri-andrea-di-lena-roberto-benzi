package model;

import java.io.File;
import java.io.IOException;

public class FileCreation {
	/**
	 * This class is used to create the hihgScore file in the Desktop direcotry of the user
	 */
	String homeDirectory=System.getProperty("user.home");
	String separator=System.getProperty("file.separator");
	String dir=homeDirectory+separator+"highScore.txt";
	
	public void createFile() {
		@SuppressWarnings("unused")
		File f= new File(dir);
		try {
			f.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
